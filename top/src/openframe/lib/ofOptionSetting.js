
Top.Controller.create('OptionSettingLogic', {
	save : function(event, widget) {
		var inputValue;
		var serverDomain;
		inputValue = window["configRepository"]["siteConfigInstance"]["serverAddress"];
		
		if(inputValue == 'default'){
			serverDomain = window.location.host;
		}else{
			serverDomain = inputValue;
		}
		
    	localStorage.setItem("ofStatus.serverDomain", inputValue);
    	localStorage.setItem("ofStatus.serverAddress","http://" + serverDomain);
        localStorage.setItem("ofStatus.serverAddressWs","ws://" + serverDomain);
        
	    sessionStorage.setItem("ofStatus.serverAddress", "http://" + serverDomain);
	    sessionStorage.setItem("ofStatus.serverAddressWs", "ws://" + serverDomain);
	}
	
});
