/*
 * 웹소켓을 정의한 파일.
 * 
 * 웹소켓의 전역 변수 및 명령어 목록,
 * 웹 소켓 연결 주소, onOpen, onMessage, onClose, onError 등 웹소켓과 관련된 동작을 정의한다.
 */
class WebSocketClient {
	
    constructor(urlString) {
        this.ws = null;
        this.urlString = urlString;
        this.sessionid = null;
    }
    
    getServerUrl() {
        return this.urlString;
    }
    
    connect() {
        try {
            this.ws = new WebSocket(this.getServerUrl());
            
            // 
            // Implement WebSocket event handlers!
            //
            this.ws.onopen = function(event) {
            	ofCommonExeModule.log('onopen::' + JSON.stringify(event, null, 4));
            }
            
            this.ws.onmessage = function(event) {
            	let msg;
            	ofCommonExeModule.log('onmessage::' + event.data);
            	if(event.data.startsWith('{')){
            		const topStatus = ofCommonStatus.getCurrent();
            		msg = JSON.parse(event.data);
            		const work = ofCommonModule.interpretJson(msg, topStatus);
			    	const nextStatus = work(msg, topStatus);
			    	ofCommonStatus.set(nextStatus);
            	}  
            }
            this.ws.onclose = function(event) {
            	ofCommonExeModule.log('onclose::' + JSON.stringify(event, null, 4));   
                if(event.code != 1000){
                	// 1000은 정상 종료. 그 이외의 값은 다양한 상황에서 발생.
                	ofCommonExeModule.log("session is closed");
                }
            }
            this.ws.onerror = function(event) {
            	ofCommonExeModule.log('onerror::' + JSON.stringify(event, null, 4));
            }
        } catch (exception) {
            console.error(exception);
        }
    }
    
    getStatus() {
        return this.ws.readyState;
    }
    
    send(message) {
    	let stringMessage;
    	if(Object.prototype.toString.call(message) !== "[object JSON]"){
    		stringMessage = JSON.stringify(message);
    	}else{
    		stringMessage = message;
    	}
    	
        if (this.ws.readyState == WebSocket.OPEN) {
        	this.ws.send(stringMessage);
        	ofCommonExeModule.log("send :: " + stringMessage);
        } else {
        	console.error('webSocket is not open. readyState=' + this.ws.readyState);
            ofCommonExeModule.log("connection is closed");
        }
    }
    disconnect() {
        if (this.ws.readyState == WebSocket.OPEN) {
            this.ws.close();
        } else {
            console.error('webSocket is not open. readyState=' + this.ws.readyState);
        }
    }
    
}
