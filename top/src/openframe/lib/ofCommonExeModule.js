const ofCommonExeModule = {
	setStartList : function(dataform){
		let startList = JSON.parse(dataform.jsonMessage);
		if(startList.length != 0){
			configRepository.setValue("startList", startList);
		}
	},
    logout : function(topStatus){
        sessionStorage.clear();
        ofCommonStatus.init();
        baseRepository.setValue("baseInstance.text", "");
        Top.App.routeTo("/LoginPage");
    },
	gotoMenu : function(topStatus){
		let nextStatus = topStatus.getClone();
		nextStatus.mapName = "";
		nextStatus.mapSetName = "";
		nextStatus.pageName = "";
		nextStatus.nextTransaction = "";
		baseRepository.setValue("baseInstance.text", "");
		Top.App.routeTo("/TranMenuPage");
		return nextStatus;
	},
    checkTextMessage : function(){
    	if(TextDialog.TextDialog.isOpen()){
    		TextDialog.TextDialog.close(); // 만약 눈에 안보이는 다이얼로그가 남아 있었있을 경우 에러 방지를 위해 종료시킨다.
    	}
    	let string = baseRepository.baseInstance.text;
    	if(string != null && string != ""){
    		TextDialog.TextDialog.open();
    	}
	},
	log : function(string){
		if(ofCommonStatus.debug == true){
			console.log(string);
		}
	}	
}
	