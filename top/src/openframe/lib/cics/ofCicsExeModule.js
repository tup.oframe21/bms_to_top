const ofCicsExeModule = {
		routeTo : function(dataform, topStatus){
			let nextStatus = topStatus.getClone();
			if(dataform.systemDto.error != null && dataform.systemDto.error != ""){
				ofCommonExeModule.log("ERROR! : " + dataform.error);
				nextStatus = ofCommonExeModule.gotoMenu(topStatus);
			}else if(dataform.screenDto.mapName == null && dataform.screenDto.mapSetName == null
					&& dataform.screenDto.text == null && dataform.systemDto.nextTransaction == null){
				/*
				 * 정상적으로 동작했지만 다음 동작 지정이 아무것도 없는 상황. 
				 * 이 경우 모든 작업이 종료되었다고 가정하고 메뉴 선택 화면으로 돌아간다.
				 */
				ofCommonExeModule.log("Transaction END");
				nextStatus = ofCommonExeModule.gotoMenu(topStatus);
			}else{
				if(dataform.screenDto.mapName == null && dataform.screenDto.mapSetName == null){
					/*
					 * do nothing. SEND MAP없이 SEND TEXT만 들어오는 케이스처럼 map과 mapset 둘 다 없이 들어오는 케이스가 존재한다.
					 * 이 경우 페이지 전환이 발생하지 않으므로 status에서 map과 mapset이름을 초기화 하지 않는다.
					 */
				}else{
					if(dataform.screenDto.mapName != null){
				        nextStatus.mapName = dataform.screenDto.mapName.replace(/[ \t]+/g,"");
				    }else{
				        nextStatus.mapName = "";
				    }
					if(dataform.screenDto.mapSetName != null){
					    nextStatus.mapSetName = dataform.screenDto.mapSetName.replace(/[ \t]+/g,"");
					}else{
					    nextStatus.mapSetName = "";
					}
				}
				if(dataform.tabId != null){
					nextStatus.tabId = dataform.tabId;
				}
				if(dataform.systemDto.nextTransaction != null){
				    nextStatus.nextTransaction = dataform.systemDto.nextTransaction.replace(/[ \t]+/g,"");
				}else{
				    nextStatus.nextTransaction = "";
				}
				if(dataform.systemDto.immediate != null){
					nextStatus.immediate = dataform.systemDto.immediate;
				}
				if(dataform.screenDto.text != null){
					baseRepository.setValue("baseInstance.text", dataform.screenDto.text);
				}else{
					baseRepository.setValue("baseInstance.text", "");
				}
				const pageName = nextStatus.mapSetName + "_" + nextStatus.mapName;
				if(dataform.screenDto[pageName + ofString.DATA] != null){
                    DataPool.setValue(pageName + ofString.POOL, dataform.screenDto[pageName + ofString.DATA]);
                }
                if(dataform.screenDto[pageName + ofString.META] == null){
                    ofCommonAttrMap = {};
                }else{
                    ofCommonAttrMap = dataform.screenDto[pageName + ofString.META];
                }
				Top.App.routeTo("/FramePage");
			}
			return nextStatus;
		}
}
