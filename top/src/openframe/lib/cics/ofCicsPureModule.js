const ofCicsPureModule = {
		genLengthField : function(StringDataPool){
			const keys = Object.keys(StringDataPool);
			let newPool = {};
			for(const key of keys){
				ofCommonExeModule.log("key = " + key);
				if(StringDataPool[key] == null){
					newPool[key + ofString.LENGTH] = 0;
				}else{
					newPool[key + ofString.LENGTH] = StringDataPool[key].trimRight().length;
				}
			}
			return newPool;
		}
}