const ofCommonService = {
		login : function(loginForm, topStatus){
			let nextStatus;
			nextStatus = ofCommonExeModule.gotoMenu(topStatus);
			//nextStatus.webSocket = new webSocketClient(nextStatus.getServerAddressWs())
			//nextStatus.webSocket.connect();
			return nextStatus;
		},logout : function(topStatus){
			const logoutForm = ofCommonPureModule.getMessageForm("logout");
			logoutForm.dto.termId = topStatus.termId;
			logoutForm.dto.tabId = topStatus.tabId;
			//topStatus.webSocket.disconnect();
			ofCommonExeModule.logout(topStatus);
			
			var xhr = new XMLHttpRequest();
			xhr.withCredentials = true;
			xhr.addEventListener("readystatechange", function() {
			  if(this.readyState === 4) {
				  if(this.status == 200){
					  ofCommonExeModule.log('success');
				  }else{
					  ofCommonExeModule.log('error');
				  }
			  }
			});
			xhr.open("POST", topStatus.getServerAddress() + '/' + 'OF21/pgmsvg/LogoutService' + '?action=');
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.send(JSON.stringify(logoutForm));
			ofCommonExeModule.log('request : ' + JSON.stringify(logoutForm));
		},
		tranExecute : function(tranUrl, dataform, topStatus, config){
			let nextStatus;
			dataform.dto.termId = topStatus.termId;
			dataform.dto.tabId = topStatus.tabId;
			dataform.dto.systemDto.croninputDto = {};
			
			var xhr = new XMLHttpRequest();
			xhr.withCredentials = true;
			xhr.addEventListener("readystatechange", function() {
			  if(this.readyState === 4) {
				  if(this.status == 200){
					  const resultJson = (JSON.parse(this.responseText)).dto;
					  ofCommonExeModule.log('response : ' + JSON.stringify(resultJson));
					  const work = ofCommonPureModule.interpretJson(resultJson, topStatus);
					  nextStatus = work(resultJson, topStatus);
					  ofCommonExeModule.log('success');
				  }else{
					  ofCommonExeModule.log('error');
					nextStatus = {};
					
				  }
				  ofCommonStatus.set(nextStatus);
			  }
			});

			xhr.open("POST", topStatus.getServerAddress() + '/' + tranUrl + '?action=');
			xhr.setRequestHeader("Content-Type", "application/json");
//			if(sessionStorage.getItem("ofStatus.jsessionId") != null && sessionStorage.getItem("ofStatus.jsessionId") != "" ){
//				xhr.setRequestHeader("Cookie", "JSESSIONID=" + sessionStorage.getItem("ofStatus.jsessionId") + "; Path=/; HttpOnly");
//			}
			xhr.send(JSON.stringify(dataform));
			ofCommonExeModule.log('request : ' + JSON.stringify(dataform));
		}
		/*
		tranExecute : function(tranUrl, dataform, topStatus, config){
			let nextStatus;
			dataform.dto.termId = topStatus.termId;
			Top.Ajax.execute({
				//url:'http://192.168.14.24:18080/openframe20Test/Svg/OIVPTEST',
			    url: topStatus.getServerAddress() + '/' + tranUrl + '?action=',
			    type: 'POST',
			    dataType: 'json',
			    data: dataform,
			    async: false,
			    contentType : 'application/json',
			    withCredentials : true,
			    success: function(ret, xhr, status){
			    	console.log(ret.dto);
			    	const resultJson = (JSON.parse(ret)).dto;
			    	const work = ofCommonPureModule.interpretJson(resultJson, topStatus);
			    	nextStatus = work(resultJson, topStatus);
			        console.log('success');
			    },
			    error: function(ret, xhr, status){
			    console.log('error');
			    nextStatus = {};
			    }
			})
			return nextStatus;
		}*/
}