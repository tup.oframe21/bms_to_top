let ofCommonAttrMap = {};
const ofString = {POOL : "_Pool", HTML : ".html", DATA : "_data", META : "_meta", LENGTH : "_length"};
/**
 * 실시간으로 자주 변경되는 status들을 모아놓는다.
 */ 
const ofRealTimeStatus = {
	/** 
	 * busy : 페이지에서 작업 요청을 보냈다면 중복으로 보내지 않도록 끊는 역할
	 */
	busy : false
}

/**
 * 페이지 단위로 변경되는 status를 모아놓는다. 일반적으로 transaction의 실행 결과로서 결정되고,
 * 새로운 기능을 호출했을 때 변경된다.
 * 초기화만 되는 항목도 있고, 주고받을때 계속 복사되는 항목도 있음.
 */
const ofCommonStatus = {
	debug : false,
	immediate : "false",
	webSocket : null,
	loadLocalStorage : function(){
		if(localStorage.getItem("ofStatus.serverDomain") == null){
			var inputValue;
			var serverDomain;
			inputValue = window["configRepository"]["siteConfigInstance"]["serverAddress"];
			
			if(inputValue == 'default'){
				serverDomain = window.location.host;
			}else{
				serverDomain = inputValue;
			}
        	localStorage.setItem("ofStatus.serverDomain", inputValue);
        	localStorage.setItem("ofStatus.serverAddress","http://" + serverDomain);
            localStorage.setItem("ofStatus.serverAddressWs","ws://" + serverDomain);
        }else{
        	window["configRepository"]["siteConfigInstance"]["serverAddress"] = localStorage.getItem("ofStatus.serverDomain");
        }
	},
    init : function(){    	
        sessionStorage.setItem("ofStatus.type","cics");
        sessionStorage.setItem("ofStatus.mapName","");
        sessionStorage.setItem("ofStatus.mapSetName","");
        sessionStorage.setItem("ofStatus.nextTransaction","");
        sessionStorage.setItem("ofStatus.jsessionId","");
        sessionStorage.setItem("ofStatus.tabId","");
        sessionStorage.setItem("ofStatus.termId",ofCommonStatus.genUUID());
        sessionStorage.setItem("ofStatus.serverAddress", localStorage.getItem("ofStatus.serverAddress"));
        sessionStorage.setItem("ofStatus.serverAddressWs", localStorage.getItem("ofStatus.serverAddressWs"));
    },
	getCurrent : function(){
		return {
			type: sessionStorage.getItem("ofStatus.type"),
			mapName: sessionStorage.getItem("ofStatus.mapName"),
			mapSetName: sessionStorage.getItem("ofStatus.mapSetName"),
			termId: sessionStorage.getItem("ofStatus.termId"),
			tabId: sessionStorage.getItem("ofStatus.tabId"),
			nextTransaction : sessionStorage.getItem("ofStatus.nextTransaction"),
			getServerAddress : this.getServerAddress,
			getServerAddressWs : this.getServerAddressWs,
			webSocket : this.webSocket,
			getClone : this.getClone,
            set: this.set
		}
	},
	getClone : function(){
        return {
            type: this.type,
            mapName: this.mapName,
            mapSetName: this.mapSetName,
            termId: this.termId,
            tabId: this.tabId,
            nextTransaction : this.nextTransaction,
            getServerAddress : this.getServerAddress,
			getServerAddressWs : this.getServerAddressWs,
			webSocket : this.webSocket,
            getClone: this.getClone,
            set: this.set
        }
    },
    getServerAddress : function(){
    	return sessionStorage.getItem("ofStatus.serverAddress");
    },
    getServerAddressWs : function(){
    	return sessionStorage.getItem("ofStatus.serverAddressWs");
    },
	/**
	 * set을 했을 때, 비어있는 항목은 초기화 되는 것이 아니고 원래의 항목이 유지된다는 점을 유의할 것. 
	 */
	set : function(status){
		if(status.type != null){
			sessionStorage.setItem("ofStatus.type",status.type);
		}
		if(status.mapName != null){
			sessionStorage.setItem("ofStatus.mapName",status.mapName);
		}
		if(status.mapSetName != null){
			sessionStorage.setItem("ofStatus.mapSetName",status.mapSetName);
		}
		if(status.nextTransaction != null){
			sessionStorage.setItem("ofStatus.nextTransaction",status.nextTransaction);
		}
		if(status.immediate != null){
			this.immediate = status.immediate;
		}
		if(ofCommonStatus.webSocket == null && status.webSocket != null){
			this.webSocket = status.webSocket;
		}
		if(sessionStorage.getItem("ofStatus.tabId") == ""){
			if(status.tabId != null){
				sessionStorage.setItem("ofStatus.tabId", status.tabId);
			}
		}
	},
	genUUID : function() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 3 | 8);
        return v.toString(16);
      });
    }
}
ofCommonStatus.loadLocalStorage();
ofCommonStatus.init();
