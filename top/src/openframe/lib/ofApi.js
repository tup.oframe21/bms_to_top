const ofApi = {
	changeAttribute : function(metaData){
        for(key in metaData){
            const slice = key.split(/_(?=[a-z]+$)/);
            if(slice.length != 2){
                console.log("meta 데이터 type 확인 실패. key : " + key);
            }
            const variable = slice[0];
            const type = slice[1];

            switch(type){
            case "length":
            	break;
            case "color":
                break;
            case "hilight":
                break;
            case "outline":
                break;
            case "transp":
                break;
            case "validn":
                break;
            case "sosi":
                break;
            case "ps":
                break;
                default:
                    console.log("meta 데이터에서 type 해석문제 발생. key : " + key + ", type : " + type);
                    break;
            }
        }
    },
    focusIC : function(){
        var widget = Top.curPage.select(".CURSOR").pop();
        if(widget != null){
            widget.focus();
        }
    },
    tranExecute : function(tranServicePath, aidKey){
    	const currentStatus = ofCommonStatus.getCurrent();
		const pageName = currentStatus.mapSetName + '_' + currentStatus.mapName;
		let dataform;
		switch(currentStatus.type){
		case "cics":
			dataform = ofCommonPureModule.getMessageForm("tranExecute");
			break;
		case "cics_of7":	
			dataform = ofCommonPureModule.getMessageForm("receiveMapOf7");
			break;	
		}
		if(ofCommonStatus.immediate != "true"){
			// immediate옵션이 켜져있는 트랜잭션은 서버로부터 전달받은 트랜잭션임. 터미널에서 시작하는 트랜잭션만 'td'를 부여받는다.
			dataform.dto.systemDto.startCode = 'TD';
		}
		try{
		    dataform.dto.systemDto.aidKey = aidKey;
            if(DataPool[pageName + ofString.POOL] != null){
                dataform.dto.screenDto[pageName + ofString.DATA] = DataPool[pageName + ofString.POOL];
                dataform.dto.screenDto[pageName + ofString.META] = ofCicsPureModule.genLengthField(DataPool[pageName + ofString.POOL]);
            }
        }catch(e){
            ofCommonExeModule.log("systemData, mapData전송 실패 :: 작업은 진행되지만 제대로 동작하는 상황인지 확인할 것.");
        }
		ofCommonService.tranExecute(tranServicePath, dataform, currentStatus);
    }

}