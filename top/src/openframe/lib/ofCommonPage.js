
Top.Controller.create('OF8ProjectLogic', {
	logout : function(event, widget) {
        ofCommonService.logout(ofCommonStatus.getCurrent());
    },
	textDialogClose : function(event, widget) {
	},
	textDialogMove : function(event, widget) {
        const dialog = Top.Dom.selectById("TextDialog");
        const x = document.querySelectorAll("div#top-dialog-root_TextDialog")[0].style.left; // return value example : "0px", "-100px"
        const y = document.querySelectorAll("div#top-dialog-root_TextDialog")[0].style.top

        try{
            if(x[0] == '-'){
                dialog.setProperties({"layoutLeft":"0px"});
            }
            if(y[0] == '-'){
                dialog.setProperties({"layoutTop":"0px"});
            }
        }catch(e){
            ofCommonExeModule.log(e);
        }
    }
});

Top.Controller.create('LoginLogic', {
	init : function(event, widget){
		ofCommonExeModule.checkTextMessage();
	},
	login : function(event, widget) {
	    const currentStatus = ofCommonStatus.getCurrent();
		const loginForm = {dto:{}};
		loginForm.dto.userid = Top.curPage.selectById("IdTextField").getText();
		loginForm.dto.passwd = Top.curPage.selectById("PasswordTextField").getText();
		loginForm.dto.termId = currentStatus.termId;

		const nextStatus = ofCommonService.login(loginForm, currentStatus);
		ofCommonStatus.set(nextStatus);
	}
});

Top.Controller.create('FrameLogic', {
	init : function(event, widget){
	    let currentStatus = ofCommonStatus.getCurrent();
		if(currentStatus.mapSetName != "" || currentStatus.mapName != ""){
			Top.Dom.selectById("InnerPanelInclude").src(currentStatus.mapSetName + "_" + currentStatus.mapName + ofString.HTML);
			document.querySelectorAll("div#content_layout")[0].style.overflow = "auto";
		}
		ofCommonExeModule.checkTextMessage();
		if(ofCommonStatus.immediate != null && ofCommonStatus.immediate == 'true'){
			ofApi.tranExecute(ofCommonStatus.nextTransaction);
		}
	},
	popUpMsg : function(event, widget) {
	}, 
	help : function(event, widget) {
	}
});

Top.Controller.create('TranMenuLogic', {
	init : function(event, widget){
		ofCommonExeModule.checkTextMessage();
		if(ofCommonStatus.immediate != null && ofCommonStatus.immediate == 'true'){
			ofApi.tranExecute(ofCommonStatus.nextTransaction);
		}
	},
	tranExecute : function(event, widget) {
		const selectBox = Top.curPage.selectById("TranSelectList");
		if(selectBox.getSelectedData() != null){
			let transaction = selectBox.getSelectedData();
			ofApi.tranExecute(transaction.url);
		}	
	}
});


/**
 * enter키를 쳤을 때의 동작을 서술한다.
 * @param e
 * @returns
 */
Top.EventManager.addShortcut('13', function(e){
	e.preventDefault();
	switch(Top.curPage.id){
	case "LoginPage":
		Top.Controller.get("LoginLogic").login();
		break;
	case "BrowsePage":
		Top.Controller.get("TranMenuLogic").tranExecute();
		break;
	case "FramePage":
		ofCommonExeModule.log("enter");
		const currentStatus = ofCommonStatus.getCurrent();
		const tranUrl = currentStatus.nextTransaction;
		if(tranUrl == ""){
			nextStatus = ofCommonExeModule.gotoMenu(currentStatus);
		}else{
			ofApi.tranExecute(tranUrl, "enter");
		}
		break;
	}
});



Top.EventManager.addShortcut('27', function(e){
	e.preventDefault();
	//var sc = Top.curPage.selectById("WebView10");
	//sc.getController();
	ofCommonExeModule.log("esc");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "clear");
});

Top.EventManager.addShortcut('112', function(e){
	e.preventDefault();
	//var sc = Top.curPage.selectById("WebView10");
	//sc.getController();
	ofCommonExeModule.log("f1");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf1");
});

Top.EventManager.addShortcut('113', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f2");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf2");
});

Top.EventManager.addShortcut('114', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f3");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf3");
});

Top.EventManager.addShortcut('115', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f4");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf4");
});

Top.EventManager.addShortcut('116', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f5");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf5");
});

Top.EventManager.addShortcut('117', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f6");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf6");
});

Top.EventManager.addShortcut('118', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f7");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf7");
});

Top.EventManager.addShortcut('119', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f8");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf8");
});

Top.EventManager.addShortcut('120', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f9");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf9");
});

Top.EventManager.addShortcut('121', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf10");
});

Top.EventManager.addShortcut('122', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f11");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf11");
});

/*Top.EventManager.addShortcut('123', function(e){
	e.preventDefault();
	ofCommonExeModule.log("f12");
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf12");
});*/

Top.EventManager.addShortcut('Shift+112', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf13");
});

Top.EventManager.addShortcut('Shift+113', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf14");
});

Top.EventManager.addShortcut('Shift+114', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf15");
});

Top.EventManager.addShortcut('Shift+115', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf16");
});

Top.EventManager.addShortcut('Shift+116', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf17");
});

Top.EventManager.addShortcut('Shift+117', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf18");
});

Top.EventManager.addShortcut('Shift+118', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf19");
});

Top.EventManager.addShortcut('Shift+119', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf20");
});

Top.EventManager.addShortcut('Shift+120', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf21");
});

Top.EventManager.addShortcut('Shift+121', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf22");
});

Top.EventManager.addShortcut('Shift+122', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf23");
});

Top.EventManager.addShortcut('Shift+123', function(e){
	e.preventDefault();
	const currentStatus = ofCommonStatus.getCurrent();
	const tranUrl = currentStatus.nextTransaction;
	ofApi.tranExecute(tranUrl, "pf24");
});
