const ofCommonPureModule = {
	getMessageForm : function(requestId) {
		const json =
		    {
		        "dto":
		        {
		            "id": requestId,
		            "tabId": "",
		            "termId":"",
                }
            };
		switch(requestId){
		case "tranExecute":
		case "receiveMapOf7":
			json.dto.screenDto = {};
			json.dto.screenDto.mapSetName = "";
			json.dto.screenDto.mapName = "";
			json.dto.systemDto = {};
			break;
		case "logout":
			//nothing
			break;
			default:
				ofCommonExeModule.log("ofCommonModule :: requestId에 맞는 메세지폼이 없습니다.");
				return null;
		}
		return json;
	},
	interpretJson : function(jsonDataDto, topStatus){
		switch(jsonDataDto.id){
		case "tranExecute":
			switch(topStatus.type){
			case "cics":
				return ofCicsExeModule.routeTo;
				break;
			case "ims":
			case "aim":
				default:
					break;
			}
			break;
		case "sendMapOf7":
			return ofCicsExeModule.routeTo;
			break;
		case "notice":
			break;
		}
		return null;
	}
}
	