#!/bin/bash
deployModulePath=`pwd`
parentdir="$(dirname "$deployModulePath")"
targetPath=target/web/top/target
java -jar top/deploy/top-generator-cli-1.0.jar --project-name top --project-path ${deployModulePath}/top --target-path ${deployModulePath}/top/target --option-isPackaging true
cp ${deployModulePath}/top/${targetPath}/* ../PipeLine/
cp top/deploy/top_deploy.sh ../PipeLine/